#include "Etat.h"

SINGLETON_DECLARATION_CPP(Etat);

bool        Etat::enmouvement = true;
GLint       Etat::modele = 1;
GLdouble    Etat::rotationY = 0.0;
GLdouble    Etat::rotationIncr = 2.0;
int         Etat::affichageStereo = 1;
