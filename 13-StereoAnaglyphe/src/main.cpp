#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-forme.h"
#include "inf2705-Singleton.h"
#include "Etat.h"
#include "Pipeline.h"
#include "Camera.h"
#include "VueStereo.h"

// les formes
FormeCube *cube = NULL;
FormeSphere *sphere = NULL;
FormeTore *tore = NULL;
FormeTheiere *theiere = NULL;
FormeIcosaedre *icosaedre = NULL;
FormeDodecaedre *dodecaedre = NULL;

void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();

    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesFragments[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments[1];
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locColor = glGetAttribLocation( prog, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
}

void FenetreTP::initialiser()
{
    glEnable( GL_DEPTH_TEST );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

    // charger les nuanceurs
    chargerNuanceurs();

    // créer quelques autres formes
    glUseProgram( prog );
    cube = new FormeCube( 2.0, false );
    sphere = new FormeSphere( 1.0, 12, 12, false );
    tore = new FormeTore( 0.01, 1.0, 10, 50, false );
    theiere = new FormeTheiere( );
    dodecaedre = new FormeDodecaedre( false );
    icosaedre = new FormeIcosaedre( false );
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    delete cube;
    delete sphere;
    delete tore;
    delete theiere;
    delete dodecaedre;
    delete icosaedre;
}

void definirProjection( int OeilMult, int w, int h ) // 0: mono, -1: oeil gauche, +1: oeil droit
{
    const GLdouble resolution = 100.0; // pixels par pouce
    GLdouble oeilDecalage = OeilMult * VueStereo::dip/2.0;
    GLdouble proportionProfondeur = VueStereo::zavant / VueStereo::zecran;  // la profondeur du plan de parallaxe nulle

    matrProj.Frustum( (-0.5 * w / resolution - oeilDecalage ) * proportionProfondeur,
                      ( 0.5 * w / resolution - oeilDecalage ) * proportionProfondeur,
                      (-0.5 * h / resolution                ) * proportionProfondeur,
                      ( 0.5 * h / resolution                ) * proportionProfondeur,
                      VueStereo::zavant, VueStereo::zarriere );
    matrProj.Translate( -oeilDecalage, 0.0, 0.0 );
}

void afficherModele()
{
    glVertexAttrib3f( locColor, 1.0, 1.0, 1.0 );
    glEnableVertexAttribArray(locColor);

    matrVisu.LookAt( VueStereo::zecran*cos(glm::radians(camera.theta))*sin(glm::radians(camera.phi)),
                     VueStereo::zecran*sin(glm::radians(camera.theta))*sin(glm::radians(camera.phi)),
                     VueStereo::zecran*cos(glm::radians(camera.phi)),
                     0, 0, 0,
                     0, 1, 0 );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    matrModel.PushMatrix();
    matrModel.Scale( VueStereo::factzoom, VueStereo::factzoom, VueStereo::factzoom );

    switch ( Etat::modele )
    {
    case 1:
        matrModel.Translate( cos(Etat::rotationY*M_PI/180.0), 0, -sin(Etat::rotationY*M_PI/180.0) );
        matrModel.Rotate( Etat::rotationY, 0.0, 0.1, 0.0 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        sphere->afficher();
        break;
    case 2:
        matrModel.Translate( cos(Etat::rotationY*M_PI/180.0), 0, -sin(Etat::rotationY*M_PI/180.0) );
        matrModel.Rotate( Etat::rotationY, 0.0, 0.1, 0.0 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        cube->afficher();
        break;
    case 3:
        {
            const GLdouble DistanceSoleilTerre = 1.0;
            const GLdouble RayonSoleil = 0.2;
            const GLdouble RayonTerre = 0.15;

            matrModel.Scale( 2.0, 2.0, 2.0 );
            matrModel.PushMatrix();{
                matrModel.Scale( RayonSoleil, RayonSoleil, RayonSoleil );
                glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
                sphere->afficher();
            }matrModel.PopMatrix();
            matrModel.Rotate( 20.0, 1, 0, 0 );
            matrModel.PushMatrix();{
                matrModel.Rotate( 90.0, 1, 0, 0 );
                glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
                tore->afficher( );
            }matrModel.PopMatrix();
            matrModel.Rotate( Etat::rotationY, 0.0, 0.1, 0.0 );
            matrModel.Translate( DistanceSoleilTerre, 0.0, 0.0 );
            glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
            matrModel.PushMatrix();{
                matrModel.Scale( RayonTerre, RayonTerre, RayonTerre );
                glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
                sphere->afficher();
            }matrModel.PopMatrix();
        }
        break;
    case 4:
        matrModel.Rotate( 40.0, 1, 0, 0 );
        matrModel.Translate( cos(Etat::rotationY*M_PI/180.0), 0, -sin(Etat::rotationY*M_PI/180.0) );
        matrModel.Rotate( Etat::rotationY, 0.0, 0.1, 0.0 );
        matrModel.Translate( -1, -1, 0 );
        matrModel.Scale( 0.5, 0.5, 0.5 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        theiere->afficher();
        break;
    case 5:
        matrModel.Translate( cos(Etat::rotationY*M_PI/180.0), 0, -sin(Etat::rotationY*M_PI/180.0) );
        matrModel.Rotate( Etat::rotationY, 0.0, 0.1, 0.0 );
        matrModel.Scale( 0.5, 0.5, 0.5 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        dodecaedre->afficher();
        break;
    case 6:
        matrModel.Translate( cos(Etat::rotationY*M_PI/180.0), 0, -sin(Etat::rotationY*M_PI/180.0) );
        matrModel.Rotate( Etat::rotationY, 0.0, 0.1, 0.0 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        icosaedre->afficher();
        break;
    }

    matrModel.PopMatrix();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
}

void FenetreTP::afficherScene(void)
{
    // glDrawBuffer( GL_BACK );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glUseProgram( prog );

    matrModel.LoadIdentity();

    switch ( Etat::affichageStereo )
    {
    case 0: // mono
        definirProjection( 0, largeur_, hauteur_ );
        glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );
        afficherModele();
        break;

    case 1: // stéréo anaglyphe
        // glDrawBuffer( GL_BACK_LEFT );
        definirProjection( -1, largeur_, hauteur_ );
        glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );
        glColorMask( GL_TRUE, GL_FALSE, GL_FALSE, GL_TRUE );
        afficherModele();

        // glDrawBuffer( GL_BACK_RIGHT );
        glClear( GL_DEPTH_BUFFER_BIT );
        definirProjection( +1, largeur_, hauteur_ );
        glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );
        glColorMask( GL_FALSE, GL_TRUE, GL_TRUE, GL_TRUE );
        afficherModele();

        glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE );
        break;
    }

    if ( Etat::enmouvement ) Etat::rotationY += Etat::rotationIncr;

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;
    case TP_v: // Recharger les fichiers des nuanceurs et recréer le programme
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;
    case TP_s: // Varier le type d'affichage stéréo: mono, stéréo anaglyphe
        Etat::affichageStereo = 1 - Etat::affichageStereo;
        std::cout << ( Etat::affichageStereo ? "Stereo" : "Mono" ) << std::endl;
        break;
    case TP_ESPACE: // Mettre en pause ou reprendre l'animation
        Etat::enmouvement = !Etat::enmouvement;
        break;
    case TP_1: // Afficher une sphère
        Etat::modele = 1; break;
    case TP_2: // Afficher un cube
        Etat::modele = 2; break;
    case TP_3: // Afficher des planètes
        Etat::modele = 3; break;
    case TP_4: // Afficher une théière
        Etat::modele = 4; break;
    case TP_5: // Afficher un dodécaèdre
        Etat::modele = 5; break;
    case TP_6: // Afficher un icosaèdre
        Etat::modele = 6; break;

    case TP_SOULIGNE:
    case TP_MOINS: // Reculer la caméra
        VueStereo::zecran -= 0.1;
        break;
    case TP_PLUS: // Avancer la caméra
    case TP_EGAL:
        VueStereo::zecran += 0.1;
        break;

    case TP_INFERIEUR:
    case TP_VIRGULE: // Diminuer la distance interpupillaire
        VueStereo::dip -= 0.01;
        if ( VueStereo::dip < 0.0 ) VueStereo::dip = 0.0;
        break;
    case TP_SUPERIEUR:
    case TP_POINT: // Augmenter la distance interpupillaire
        VueStereo::dip += 0.01;
        break;

    case TP_CROCHETGAUCHE: // Diminuer le facteur de zoom
        VueStereo::factzoom -= 0.1;
        if ( VueStereo::factzoom < 0.1 ) VueStereo::factzoom = 0.1;
        break;
    case TP_CROCHETDROIT: // Augmenter le facteur de zoom
        VueStereo::factzoom += 0.1;
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }

    std::cout << " dip=" << VueStereo::dip << " factzoom=" << VueStereo::factzoom
              << " zavant=" << VueStereo::zavant << " zecran=" << VueStereo::zecran << " zarriere=" << VueStereo::zarriere << std::endl;
}

// fonction callback pour un clic de souris
glm::ivec2 sourisPosPrec(0,0);
static bool presse = false;
void FenetreTP::sourisClic( int button, int state, int x, int y )
{
    presse = ( state == TP_PRESSE );
    if ( presse )
    {
        sourisPosPrec.x = x;
        sourisPosPrec.y = y;
    }
}

void FenetreTP::sourisMolette( int x, int y )
{
    const int sens = +1;
    VueStereo::factzoom += 0.01 * sens * y;
    if ( VueStereo::factzoom < 0.01 ) VueStereo::factzoom = 0.01;
}

// fonction de mouvement de la souris
void FenetreTP::sourisMouvement( int x, int y )
{
    if ( presse )
    {
        int dx = x - sourisPosPrec.x;
        int dy = y - sourisPosPrec.y;
        camera.theta -= dx / 3.0;
        camera.phi   -= dy / 3.0;

        sourisPosPrec.x = x;
        sourisPosPrec.y = y;
    }
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "stereoAnaglyphe", 500, 500 );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
