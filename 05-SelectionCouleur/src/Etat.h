#ifndef __ETAT_H__
#define __ETAT_H__

#include <glm/glm.hpp>
#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static int enSelection; // on est en mode sélection?
    static glm::ivec2 sourisPosPrec;
};

#endif
