#!/bin/sh

TMP=`mktemp`
if [ "$1" == "" ]; then # TP
    ( echo "Touches possibles :";
      cat src/main.cpp | grep '^ *case.*TP_.*//' | sed 's/.*case.*TP_//; s!//!!; s/^/    /;';
      cat src/main.cpp | grep 'FenetreTP::sourisMolette' | sed 's/.*( int x, int y )/Molette:/; s!//!!; s/^/    /;' | grep -v 'Molette:$'
    ) >${TMP}; diff src/touches.txt ${TMP} && rm ${TMP} || mv ${TMP} src/touches.txt
    if [ -d sol ]; then
        ( echo "Touches possibles :";
          cat sol/main.cpp | grep '^ *case.*TP_.*//' | sed 's/.*case.*TP_//; s!//!!; s/^/    /;';
          cat sol/main.cpp | grep 'FenetreTP::sourisMolette' | sed 's/.*( int x, int y )/Molette:/; s!//!!; s/^/    /;' | grep -v 'Molette:$'
        ) >${TMP}; diff sol/touches.txt ${TMP} && rm ${TMP} || mv ${TMP} sol/touches.txt
        grep -v "Touches possibles :" src/touches.txt | ( sed 's/ *\([^:]*\):/\\texttt{\1} \&/; s/$/ \\\\/; s/_/ /g' ) >touches.txt
    fi
fi

 # BAS
 # HAUT
 # PAGEPREC
 # PAGESUIV
 # DEBUT
 # FIN
 # DROITE
 # GAUCHE
 # s/ECHAP:/ECHAP:/
 # s/EGAL:/EGAL (=):/
 # s/SUPERIEUR:/SUPERIEUR (>):/
 # s/INFERIEUR:/INFERIEUR (<):/
 # s/PLUS:/PLUS (+):/
 # s/MOINS:/MOINS (-):/
 # s/CROCHETDROIT:/CROCHETDROIT (]):/
 # s/CROCHETGAUCHE:/CROCHETGAUCHE ([):/
 # s/POINT:/POINT (.):/
 # s/VIRGULE:/VIRGULE (,):/
 # s/POINTVIRGULE:/POINTVIRGULE (;):/
 # s/BARREOBLIQUE:/BARREOBLIQUE (\/):/
 # s/SOULIGNE:/SOULIGNE (_):/
 # s/ESPACE:/ESPACE (" "):/
