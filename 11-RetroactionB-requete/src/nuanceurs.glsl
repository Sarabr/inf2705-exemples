// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS_RETROACTION)

uniform float temps;

in vec4 Vertex;
out vec4 VertexMod;

void main( void )
{
    // copier dans la variable de sortie
    VertexMod = Vertex;
    // faire quelques modifications (des « calculs » simples)
    VertexMod.y = VertexMod.y * 10;
    VertexMod.z = VertexMod.z + temps;
}

#endif
