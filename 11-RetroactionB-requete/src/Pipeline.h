#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint progRetroaction;
GLint locVertex;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;
GLint loctemps;

GLuint vao[1];   // un VAO
GLuint vbo[1];   // les VBO de cet exemple

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
