#include <stdlib.h>
#include <iostream>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "Pipeline.h"

void chargerNuanceurs()
{
    // créer le programme
    progRetroaction = glCreateProgram();

    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS_RETROACTION\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( progRetroaction, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }

    // dire quelle variable on veut récupérer
    const GLchar* vars[] = { "VertexMod" };
    glTransformFeedbackVaryings( progRetroaction, 1, vars, GL_INTERLEAVED_ATTRIBS );

    // faire l'édition des liens du programme
    glLinkProgram( progRetroaction );
    ProgNuanceur::afficherLogLink( progRetroaction );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( progRetroaction, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
}

glm::vec4 valeurs[] = { { 11., 12., 13., 1. },
                        { 21., 22., 23., 1. },
                        { 31., 32., 33., 1. },
                        { 41., 42., 43., 1. },
                        { 51., 52., 53., 1. } };

GLuint vboTransformation;
GLuint requete;

void FenetreTP::initialiser()
{
    // charger les nuanceurs
    chargerNuanceurs();

    // créer le VAO
    glGenVertexArrays( 1, vao );
    glBindVertexArray( vao[0] );

    // créer le VBO
    glGenBuffers( 1, vbo );
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(valeurs), valeurs, GL_STATIC_DRAW );

    glEnableVertexAttribArray( locVertex );
    glVertexAttribPointer( locVertex, 4, GL_FLOAT, GL_FALSE, 0, 0 );

    // créer le tampon de retour (rétroaction)
    glGenBuffers( 1, &vboTransformation );
    glBindBuffer( GL_ARRAY_BUFFER, vboTransformation );
    glBufferData( GL_ARRAY_BUFFER, sizeof(valeurs), NULL, GL_STATIC_READ );

    // créer la requête afin d'obtenir un retour d'information
    glGenQueries( 1, &requete );
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteQueries( 1, &requete );
}

void FenetreTP::afficherScene()
{
    glUseProgram( progRetroaction );
    glUniform1f( loctemps, 0.01 );

    // dire où seront stockés les résultats
    glBindBufferBase( GL_TRANSFORM_FEEDBACK_BUFFER, 0, vboTransformation );

    // débuter la requête
    glBeginQuery( GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, requete );

    // désactiver le tramage
    glEnable( GL_RASTERIZER_DISCARD );
    // débuter la rétroaction
    glBeginTransformFeedback( GL_POINTS );
    // « dessiner » (en utilisant le vbo[0])
    glDrawArrays( GL_POINTS, 0, 5 );
    // terminer la rétroaction
    glEndTransformFeedback();
    // réactiver le tramage
    glDisable( GL_RASTERIZER_DISCARD );

    // terminer la requête
    glEndQuery( GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN );

    glFlush(); // attendre que la carte grapohique ait terminé le traitement
    // obtenir et imprimer les résultats
    GLuint nresul;
    glGetQueryObjectuiv( requete, GL_QUERY_RESULT, &nresul );

    glm::vec4 tamponRetour[sizeof(valeurs)/sizeof(glm::vec4)];
    glGetBufferSubData( GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(tamponRetour), tamponRetour );

    std::cout << " nresul=" << nresul << std::endl;
    for ( unsigned int i = 0; i < nresul; ++i )
        std::cout << " valeurs[]=" << glm::to_string(valeurs[i]) << " tamponRetour[]=" << glm::to_string(tamponRetour[i]) << std::endl;
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "retro-requete" );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    //bool boucler = true;
    //while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        //boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
