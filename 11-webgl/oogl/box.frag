precision mediump float;

uniform sampler2D Texture;

varying vec2 ex_TexCoord;

void main() 
{
   vec4 couleur = texture2D( Texture, ex_TexCoord );
   if ( couleur.r < .1 && couleur.g < .1 && couleur.b < .1 ) couleur.r = 1.0;
   gl_FragColor = couleur;
}
