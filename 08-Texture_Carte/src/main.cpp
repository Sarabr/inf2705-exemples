#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-texture.h"

GLuint prog;
GLint locVertex, loclargeur, lochauteur;
GLint locmatrModel, locmatrVisu, locmatrProj;
GLint loclaTextureCarte, loclaTextureBallon, loclaTextureBonhomme;

// matrices de du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

// la fonction habituelle!
void chargerNuanceurs()
{
   // créer le programme
   prog = glCreateProgram();

   // attacher le nuanceur de sommets
   const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
   if ( chainesSommets[1] != NULL )
   {
      GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
      glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
      glCompileShader( nuanceurObj );
      glAttachShader( prog, nuanceurObj );
      ProgNuanceur::afficherLogCompile( nuanceurObj );
      delete [] chainesSommets[1];
   }
   // attacher le nuanceur de fragments
   const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
   if ( chainesFragments[1] != NULL )
   {
      GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
      glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
      glCompileShader( nuanceurObj );
      glAttachShader( prog, nuanceurObj );
      ProgNuanceur::afficherLogCompile( nuanceurObj );
      delete [] chainesFragments[1];
   }
   // faire l'édition des liens du programme
   glLinkProgram( prog );
   ProgNuanceur::afficherLogLink( prog );

   // demander la "Location" des variables
   if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
   if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
   if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
   if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
   if ( ( loclaTextureCarte = glGetUniformLocation( prog, "laTextureCarte" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureCarte" << std::endl;
   if ( ( loclaTextureBallon = glGetUniformLocation( prog, "laTextureBallon" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureBallon" << std::endl;
   if ( ( loclaTextureBonhomme = glGetUniformLocation( prog, "laTextureBonhomme" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureBonhomme" << std::endl;
   if ( ( loclargeur = glGetUniformLocation( prog, "largeur" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de largeur" << std::endl;
   if ( ( lochauteur = glGetUniformLocation( prog, "hauteur" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de hauteur" << std::endl;

   FenetreTP::VerifierErreurGL("fin chargerNuanceurs");
}

bool chargerTexture( std::string fichier, GLuint &texture )
{
   GLsizei largeur, hauteur;
   unsigned char *pixels;
   if ( ( pixels = ChargerImage( fichier, largeur, hauteur ) ) != NULL )
   {
      glGenTextures( 1, &texture );
      glBindTexture( GL_TEXTURE_2D, texture );
      glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB,
                    largeur, hauteur, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
      glBindTexture( GL_TEXTURE_2D, 0 );
      delete[] pixels;
   }
   return true;
}

void initialiser()
{
   // charger les trois textures et retourner son identificateur dans maTexture...
   GLuint maTextureCarte=0, maTextureBallon=0, maTextureBonhomme=0;
   chargerTexture( std::string("textures/carte.bmp"), maTextureCarte );
   chargerTexture( std::string("textures/ballon.bmp"), maTextureBallon );
   chargerTexture( std::string("textures/bonhomme.bmp"), maTextureBonhomme );

   // assigner chaque image dans une unité de texture différente
   glActiveTexture( GL_TEXTURE0 ); // l'unité de texture 0
   glBindTexture( GL_TEXTURE_2D, maTextureCarte );
   glActiveTexture( GL_TEXTURE1 ); // l'unité de texture 1
   glBindTexture( GL_TEXTURE_2D, maTextureBallon );
   glActiveTexture( GL_TEXTURE2 ); // l'unité de texture 2
   glBindTexture( GL_TEXTURE_2D, maTextureBonhomme );

   // charger les nuanceurs (tous vos fichiers de nuanceurs seront chargés ici)
   chargerNuanceurs();

   // Le modèle
   const GLfloat coords[] = { -1.0, -1.0,   1.0, -1.0,   1.0,  1.0,   -1.0,  1.0 };
   const GLuint connec[] = {  0, 1, 2,   2, 3, 0  };

   // allouer les objets OpenGL
   GLuint vao[1];  glGenVertexArrays( 1, vao );
   GLuint vbo[3];  glGenBuffers( 3, vbo );

   // initialiser le VAO
   glBindVertexArray( vao[0] );

   // charger le VBO pour les sommets
   glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
   glBufferData( GL_ARRAY_BUFFER, sizeof(coords), coords, GL_STATIC_DRAW );
   glVertexAttribPointer( locVertex, 2, GL_FLOAT, GL_FALSE, 0, 0 );
   glEnableVertexAttribArray(locVertex);

   // charger le VBO pour la connectivité
   glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[1] );
   glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connec), connec, GL_STATIC_DRAW );
}

void FenetreTP::afficherScene()
{
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   // assigner les variables uniformes
   glUseProgram( prog );
   glUniform1i( loclaTextureCarte, 0 ); // unité de texture 0
   glUniform1i( loclaTextureBallon, 1 ); // unité de texture 1
   glUniform1i( loclaTextureBonhomme, 2 ); // unité de texture 2
   glUniform1i( loclargeur, largeur_ ); // les dimensions de la fenêtre
   glUniform1i( lochauteur, hauteur_ ); // les dimensions de la fenêtre

   // transformation de projection orthogonale
   matrProj.Ortho( -1.0, 1.0, -1.0, 1.0, -1.0, 1.0 );
   glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

   // transformation de visualisation
   matrVisu.LookAt( 0.0, 0.0, 0.0,  0.0, 0.0, -1.0,  0.0, 1.0, 0.0 );
   glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

   // transformation de modélisation
   matrModel.LoadIdentity();
   glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

   // afficher les deux triangles qui forment le carré (6 indices dans connec[])
   glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
   glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
   switch ( touche )
   {
   case TP_ECHAP:
   case TP_q:
      quit();
      break;

   case TP_v: // Recharger les nuanceurs
      chargerNuanceurs();
      std::cout << "// Recharger nuanceurs" << std::endl;
      break;

   case TP_g:
      {
         static GLint modePlein;
         glGetIntegerv( GL_POLYGON_MODE, &modePlein );
         if ( modePlein == GL_LINE )
            glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
         else
            glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
      }
      break;

   default:
      std::cout << " touche inconnue : " << (char) touche << std::endl;
      break;
   }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{ }

void FenetreTP::sourisMolette( int x, int y )
{ }

void FenetreTP::sourisMouvement( int x, int y )
{ }

int main( int argc, char *argv[] )
{
   // créer une fenêtre
   FenetreTP fenetre( "texture", 600, 400 );

   // allouer des ressources et définir le contexte OpenGL
   initialiser();

   bool boucler = true;
   while ( boucler )
   {
      // affichage
      fenetre.afficherScene();
      fenetre.swap();

      // récupérer les événements et appeler la fonction de rappel
      boucler = fenetre.gererEvenement();
   }
   return 0;
}
