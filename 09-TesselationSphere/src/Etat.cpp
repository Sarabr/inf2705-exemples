#include "Etat.h"

SINGLETON_DECLARATION_CPP(Etat);

bool Etat::enmouvement = false;
bool Etat::deformerSphere = false;
GLdouble Etat::angle = 0.0;
GLfloat Etat::TessLevelInner = 1;
GLfloat Etat::TessLevelOuter = 1;
