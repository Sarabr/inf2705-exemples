// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;

uniform float temps;

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

out Attribs {
    vec4 couleurAvant;
    vec4 couleurArriere;
} AttribsOut;

void main(void)
{
    mat4 Rotation = mat4(  cos(temps),  sin(temps),  0.0, 0.0,
                           -sin(temps),  cos(temps),  0.0, 0.0,
                           0.0, 0.0,  1.0, 0.0,
                           0.0, 0.0,  0.0, 1.0
                           );
    // appliquer la transformation standard du sommet (P * V * M * sommet)
    gl_Position = matrProj * matrVisu * matrModel * (Rotation*Vertex);

    // assigner la couleur du sommet
    vec4 ColorMod = Rotation * (Color-0.5) + 0.5;
    AttribsOut.couleurAvant = ColorMod;
    AttribsOut.couleurArriere = 1.0 - ColorMod;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

in Attribs {
    vec4 couleurAvant;
    vec4 couleurArriere;
} AttribsIn;

out vec4 FragColor;

void main(void)
{
    // assigner la couleur du fragment qui est la couleur interpolée
    vec4 coul = gl_FrontFacing ? AttribsIn.couleurAvant : AttribsIn.couleurArriere;
    FragColor = coul;
}

#endif
