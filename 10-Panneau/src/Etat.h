#ifndef __ETAT_H__
#define __ETAT_H__

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static bool affichePoints;      // afficher des points ou des triangles?
    static float pointsize;         // la taille des points
    static int texnumero;           // numéro de la texture utilisée: 0-aucune, 1-lutin, 2-echiquier
    static GLuint textureLUTIN;     // la texture lutin
    static GLuint textureECHIQUIER; // la texture echiquier
};

#endif
