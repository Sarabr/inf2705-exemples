// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform float pointsize;

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

out Attribs {
    vec4 couleur;
} AttribsOut;

void main( void )
{
    // appliquer la transformation standard du sommet
    //gl_Position = matrProj * matrVisu * matrModel * Vertex;
    // appliquer la transformation standard du sommet ** sans la projection **
    gl_Position =            matrVisu * matrModel * Vertex;

    // couleur du sommet
    AttribsOut.couleur = Color;

    // assigner la taille des points (en pixels)
    gl_PointSize = pointsize;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_GEOMETRIE_POINTS)

layout(points) in;
layout(points, max_vertices = 1) out;

// in gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[];

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
    vec2 texCoord;
} AttribsOut;

uniform mat4 matrProj;

void main()
{
    // assigner la position du point
    gl_Position = matrProj * gl_in[0].gl_Position; // on termine la transformation débutée dans le nuanceur de sommets
    //gl_Position += vec4( 0.0, -0.5, 0.0, 0.0 );

    // assigner la taille des points (en pixels)
    gl_PointSize = gl_in[0].gl_PointSize;

    // assigner les autres attributs
    AttribsOut.couleur = AttribsIn[0].couleur;
    AttribsOut.texCoord = vec2( 0.0, 0.0 );
    EmitVertex();
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_GEOMETRIE_TRIANGLES)

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

// in gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize; // (gl_PointSize n'est pas initialisé si on ne produit pas de point)
//   float gl_ClipDistance[];
// } gl_in[];

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
    vec2 texCoord;
} AttribsOut;

uniform mat4 matrProj;

void main()
{
    vec2 coins[4];
    coins[0] = vec2( -0.5,  0.5 );
    coins[1] = vec2( -0.5, -0.5 );
    coins[2] = vec2(  0.5,  0.5 );
    coins[3] = vec2(  0.5, -0.5 );
    // à partir du point, créer quatre points qui servent de coin aux deux triangles
    for ( int i = 0 ; i < 4 ; ++i )
    {
        // On utilise la valeur de gl_PointSize pour dimensionner le panneau.
        // Dans le cas présent, considérant que les unités du repère de la
        // caméra varient entre -1 et 1, la valeur de gl_PointSize (au
        // départe initialisée à 10 pixels) devra être divisée par un facteur
        // quelconque pour la ramener dans des unités similaires à celle de
        // gl_Position (dans le repère de modélisation).
        // J'ai choisi arbitrairement d'utilise un facteur de 50.
        float fact = gl_in[0].gl_PointSize / 50;

        vec2 decalage = coins[i]; // on positionne successivement aux quatre coins
        vec4 pos = vec4( gl_in[0].gl_Position.xy + fact * decalage, gl_in[0].gl_Position.zw );

        gl_Position = matrProj * pos;    // on termine la transformation débutée dans le nuanceur de sommets

        AttribsOut.couleur = AttribsIn[0].couleur;
        AttribsOut.texCoord = coins[i] + vec2( 0.5, 0.5 ); // on utilise coins[] pour définir des coordonnées de texture
        EmitVertex();
    }
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

// in vec4 gl_FragCoord;   // <-- déclaration implicite
// in bool gl_FrontFacing; // <-- déclaration implicite
// in vec2 gl_PointCoord;  // <-- déclaration implicite
// out float gl_FragDepth; // <-- déclaration implicite

uniform sampler2D laTexture;
uniform int texnumero;

in Attribs {
    vec4 couleur;
    vec2 texCoord;
} AttribsIn;

out vec4 FragColor;

void main(void)
{
    FragColor = AttribsIn.couleur;

    // doit-on utiliser la texture?
    if ( texnumero != 0 )
    {
        vec4 couleur = texture( laTexture, AttribsIn.texCoord );
        FragColor *= couleur;
    }
}

#endif
