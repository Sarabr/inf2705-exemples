#include "Etat.h"

SINGLETON_DECLARATION_CPP(Etat);

bool       Etat::enPerspective = true;
bool       Etat::enmouvement = false;
bool       Etat::afficheAxes = true;
bool       Etat::echoTransformations = false;
int        Etat::nlignes = 0;
float      Etat::dt = float(0.01);
