#ifndef __ETAT_H__
#define __ETAT_H__

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static bool enPerspective;   // indique si on est en mode Perspective (true) ou Ortho (false)
    static bool enmouvement;         // le modèle est en mouvement/rotation automatique ou non
    static bool afficheAxes;     // indique si on affiche les axes
    static bool echoTransformations; // echo des transformations
    static int nlignes;  // nombre de lignes a traiter: 0: tout, 1...: seulement les lignes 1 à nlignes
    static float dt;                 // intervalle entre chaque affichage (en secondes)
};

#endif
