#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog;
GLint locVertex;
GLint locNormal;
GLint locColor;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;
GLint locmatrNormale;
GLuint indFrontMaterial;

GLuint vao[2];
GLuint vbo[4];
GLuint ubo[3];

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
