// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel, matrVisu, matrProj;

in vec4 Vertex;
in vec2 TexCoord;
out vec2 texCoord;

void main( void )
{
    // appliquer la transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // transmettre au nuanceur de fragments les coordonnées de texture reçues
    texCoord = TexCoord;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

uniform sampler2D laTextureTulipes, laTextureBallon, laTextureEchiquier;

uniform ivec2 poscour;
uniform int solution;

// in vec4 gl_FragCoord;
in vec2 texCoord;
out vec4 FragColor;

void main( void )
{
    if ( solution == 0 )
    {
        FragColor = texture( laTextureTulipes, texCoord );
    }
    else if ( solution == 1 )
    {
        // mélange tulipes et échiquier
        FragColor = clamp( texture( laTextureTulipes, texCoord ) + texture( laTextureEchiquier, texCoord ), 0.0, 1.0 );
    }
    else if ( solution == 2 )
    {
        // mélanger echiquier et ballon
        FragColor = clamp( texture( laTextureEchiquier, texCoord ) * texture( laTextureBallon, 8*texCoord ), 0.0, 1.0 );
    }
    else if ( solution == 3 )
    {
        // voir les tulipes seulement dans un cercle centré
        FragColor = distance( texCoord, vec2(0.5,0.5) ) < 0.5 ? texture( laTextureTulipes, texCoord ) : vec4( 0.5 );
    }
    else if ( solution == 4 )
    {
        // mauvais "flash" lors de la prise de photo!
        FragColor = mix( vec4(1.0), texture( laTextureTulipes, texCoord ), texCoord.s );
    }
    else if ( solution == 5 )
    {
        // inversion des couleurs rouge et bleu
        FragColor = texture( laTextureTulipes, texCoord ).bgra;
    }
}

#endif
