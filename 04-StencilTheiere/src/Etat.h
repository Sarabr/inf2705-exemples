#ifndef __ETAT_H__
#define __ETAT_H__

#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static float pos;    // position du plan de coupe
    static int contenu;  // 1: rectangle seulement, 2: et théière bleu, 3: et théière rouge (c'est-à-dire tout)
};

#endif
