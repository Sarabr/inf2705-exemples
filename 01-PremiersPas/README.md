# Quatre constatations (exemples en version 2.x plus « pédagogique »):

1. On déplace le repère avant de tracer une primitive dans ce repère :

![afficherModele1](images/afficherModele1.png) [afficherModele1.cpp](src/pas/afficherModele1.cpp)

2. La carte graphique interpole les attributs: sommet, couleur, etc. :

![afficherModele2](images/afficherModele2.png) [afficherModele2.cpp](src/pas/afficherModele2.cpp)

3. Pour animer une scène, on affiche constamment :

![afficherModele3](images/afficherModele3.png) [afficherModele3.cpp](src/pas/afficherModele3.cpp) et [calculerPhysique.cpp](src/pas/calculerPhysique.cpp)

4. Les objets affichés sont creux :

![afficherModele4](images/afficherModele4.png) [afficherModele4.cpp](src/pas/afficherModele4.cpp) et [calculerPhysique.cpp](src/pas/calculerPhysique.cpp)

Le programme principal qui utilise ces quatre exemples : [main.cpp](src/main.cpp).
