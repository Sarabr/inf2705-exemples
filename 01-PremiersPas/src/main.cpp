#include <cstdlib>
#if defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glut.h>
#include <glm/glm.hpp>
#include "Etat.h"

void initialisation( )
{
    glm::vec4 couleurFond( 0.0, 0.0, 0.0, 1.0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );
    glEnable( GL_DEPTH_TEST );

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    glFrustum( -3.0, 3.0, -3.0, 3.0, 4.0, 10.0 );
    glMatrixMode( GL_MODELVIEW );
}

// les différentes version de afficherModele() :
#include "pas/afficherModele1.cpp"
#include "pas/afficherModele2.cpp"
#include "pas/afficherModele3.cpp"
#include "pas/afficherModele4.cpp"

#include "pas/calculerPhysique.cpp"

void afficherScene()
{
    // afficher
    switch ( Etat::pas )
    {
    case 1: afficherModele1(); break;
    case 2: afficherModele2(); break;
    case 3: afficherModele3(); break;
    case 4: afficherModele4(); break;
    }
    glutSwapBuffers ( );

    // mettre à jour la physique
    calculerPhysique( );
}

void redimensionnement( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void clavier( unsigned char touche, int x, int y )
{
    switch ( touche )
    {
    case 27:
    case 'q':
        exit( 0 );
        break;

    case 'r': // réinitiliaser la caméra
        Etat::rtri = Etat::rquad = 0.0;
        break;

    case ' ':
        Etat::enmouvement = !Etat::enmouvement;
        break;

    case 'f':
        {
            static bool pleinecran = false;
            pleinecran = !pleinecran;
            if ( pleinecran ) glutFullScreen( ); else glutReshapeWindow( 500, 500 );
        }
        break;

    case '1': Etat::pas = 1; break;
    case '2': Etat::pas = 2; break;
    case '3': Etat::pas = 3; break;
    case '4': Etat::pas = 4; break;

    default:
        break;
    }
}

int main ( int argc, char** argv )
{
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE );
    glutInitWindowSize( 500, 500 );
    glutCreateWindow( "premiers pas" );
    initialisation();
    glutDisplayFunc( afficherScene );
    glutReshapeFunc( redimensionnement );
    glutKeyboardFunc( clavier );
    glutIdleFunc( afficherScene );
    glutMainLoop( );
    return(0);
}
