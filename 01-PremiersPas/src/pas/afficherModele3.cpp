void afficherModele3()
{
    // « effacer » l'écran
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // initiliaser la position du repère
    glLoadIdentity( );

    // déplacer le repére
    glTranslatef( -1.5, 0.0, -6.0 ); // vers la gauche
    glRotatef( Etat::rtri, 0.0, 1.0, 0.0 ); // rotation de rtri degrés autour de (0,1,0)

    // afficher le triangle
    glBegin( GL_TRIANGLES );
      glColor3f( 1.0, 0.0, 0.0 );
      glVertex3f(  0.0,  1.0, 0.0 );
      glColor3f( 0.0, 1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 0.0 );
      glColor3f( 0.0, 0.0, 1.0 );
      glVertex3f ( 1.0, -1.0, 0.0 );
    glEnd( );

    // déplacer le repére
    glLoadIdentity( ); // très mauvais de réinitiliaser ainsi, mais pratique!
    glTranslatef( 1.5, 0.0, -6.0 ); // vers la droite
    glRotatef( Etat::rquad, 1.0, 0.0, 0.0 ); // rotation de rquad degrés autour de (1,0,0)

    // afficher le quad coloré
    glColor3f( 0.5, 0.5, 1.0 );
    glBegin( GL_QUADS );
      glVertex3f( -1.0,  1.0, 0.0 );
      glVertex3f(  1.0,  1.0, 0.0 );
      glVertex3f(  1.0, -1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 0.0 );
    glEnd( );
}
