void afficherModele1()
{
    // « effacer » l'écran
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // donner la couleur des primitives
    glColor3f( 1.0, 1.0, 1.0 );

    // initiliaser la position du repère
    glLoadIdentity( );

    // se déplacer vers la gauche (et, accessoirement, vers l'arrière)
    glTranslatef( -1.5, 0.0, -6.0 );

    // afficher le triangle
    glBegin( GL_TRIANGLES );
      glVertex3f(  0.0,  1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 0.0 );
      glVertex3f(  1.0, -1.0, 0.0 );
    glEnd( );

    // déplacer le repère vers la droite
    glTranslatef( 3.0, 0.0, 0.0 );

    // afficher le quad
    glBegin( GL_QUADS );
      glVertex3f( -1.0,  1.0, 0.0 );
      glVertex3f(  1.0,  1.0, 0.0 );
      glVertex3f(  1.0, -1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 0.0 );
    glEnd( );
}
