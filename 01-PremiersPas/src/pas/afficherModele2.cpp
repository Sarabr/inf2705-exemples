void afficherModele2()
{
    // « effacer » l'écran
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // initiliaser la position du repère
    glLoadIdentity( );

    // déplacer le repère vers la gauche (et, accessoirement, vers l'arrière)
    glTranslatef( -1.5, 0.0, -6.0 );

    // afficher le triangle coloré
    glBegin( GL_TRIANGLES );
      glColor3f( 1.0, 0.0, 0.0 );
      glVertex3f(  0.0,  1.0, 0.0 );
      glColor3f( 0.0, 1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 0.0 );
      glColor3f( 0.0, 0.0, 1.0 );
      glVertex3f(  1.0, -1.0, 0.0 );
    glEnd( );

    // déplacer le repère vers la droite
    glTranslatef( 3.0, 0.0, 0.0 );

    // afficher le quad coloré
    glColor3f( 0.5, 0.5, 1.0 );
    glBegin( GL_QUADS );
      glVertex3f( -1.0,  1.0, 0.0 );
      glVertex3f(  1.0,  1.0, 0.0 );
      glVertex3f(  1.0, -1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 0.0 );
    glEnd( );
}
