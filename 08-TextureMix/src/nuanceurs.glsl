// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel, matrVisu, matrProj;

in vec4 Vertex;

void main( void )
{
    // appliquer la transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

uniform sampler2D laTextureScene, laTexturePluie;
uniform ivec2 poscour; // la position courante de la souris
uniform ivec2 taillecour; // la taille courante de la fenêtre

// in vec4 gl_FragCoord;
out vec4 FragColor;

void main( void )
{
    // calculer les coordonnées de texture
    vec2 texCoord = gl_FragCoord.xy / taillecour;

    // obtenir la couleur de la texture
    FragColor = texture( laTextureScene, texCoord );
    // s'il y a lieu, mélanger avec la texture de pluie
    if ( gl_FragCoord.x < poscour.x )
        FragColor = mix( FragColor, texture( laTexturePluie, texCoord ), 0.4 );
}

#endif
