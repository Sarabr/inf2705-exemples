// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////

// Définition des paramètres des matériaux
layout (std140) uniform MaterialParameters
{
    vec4 emission;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} FrontMaterial;

////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform mat3 matrNormale;

layout(location=0) in vec4 Vertex;
layout(location=2) in vec3 Normal;

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

out Attribs {
    vec3 lumiDir;
    vec3 normale, obsVec;
} AttribsOut;

void main(void)
{
    // appliquer la transformation standard du sommet (P * V * M * sommet)
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // calculer la normale qui sera interpolée pour le nuanceur de fragments
    AttribsOut.normale = matrNormale * Normal;

    // calculer la position du sommet (dans le repère de la caméra)
    vec3 pos = vec3( matrVisu * matrModel * Vertex );

    // calculer le vecteur de la direction de la lumière (dans le repère de la caméra)
    AttribsOut.lumiDir = vec3( 0.0, 0.0, 1.0 ); // on considère que la source de lumière est à l'infini dans la direction (0,0,1)

    // calculer le vecteur de la direction vers l'observateur
    AttribsOut.obsVec = vec3( 0.0, 0.0, 1.0 ); // on considère que l'observateur (la caméra) est à l'infini dans la direction (0,0,1)
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

const bool utiliseBlinn = true;

in Attribs {
    vec3 lumiDir;
    vec3 normale, obsVec;
} AttribsIn;

out vec4 FragColor;

void main(void)
{
    vec3 L = normalize( AttribsIn.lumiDir ); // vecteur vers la source lumineuse
    //vec3 N = normalize( AttribsIn.normale ); // vecteur normal
    vec3 N = normalize( gl_FrontFacing ? AttribsIn.normale : -AttribsIn.normale );
    vec3 O = normalize( AttribsIn.obsVec );  // position de l'observateur

    vec4 coul = FrontMaterial.emission;

    // calculer la composante ambiante
    coul += FrontMaterial.ambient;

    // calculer l'éclairage seulement si le produit scalaire est positif
    float NdotL = max( 0.0, dot( N, L ) );
    if ( NdotL > 0.0 )
    {
        // calculer la composante diffuse
        coul += FrontMaterial.diffuse * NdotL;

        // calculer la composante spéculaire (Blinn ou Phong : spec = BdotN ou RdotO )
        float spec = ( utiliseBlinn ?
                       dot( normalize( L + O ), N ) : // dot( B, N )
                       dot( reflect( -L, N ), O ) ); // dot( R, O )
        if ( spec > 0 ) coul += FrontMaterial.specular * pow( spec, FrontMaterial.shininess );
    }

    // seuiller chaque composante entre 0 et 1 et assigner la couleur finale du fragment
    FragColor = clamp( coul, 0.0, 1.0 );
}

#endif
