#include "Etat.h"

SINGLETON_DECLARATION_CPP(Etat);

bool Etat::enmouvement = false;
bool Etat::deformer = false;
GLdouble Etat::rtri = 0.;
GLdouble Etat::rquad = 0.;
GLfloat Etat::TessLevelInner = 1;
GLfloat Etat::TessLevelOuter = 1;
