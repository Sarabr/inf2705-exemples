#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint progT;
GLint locTVertex;
GLint locTColor;
GLint locTmatrModel;
GLint locTmatrVisu;
GLint locTmatrProj;
GLint locTTessLevelInner;
GLint locTTessLevelOuter;
GLint locTdeformer;
GLuint progQ;
GLint locQVertex;
GLint locQColor;
GLint locQmatrModel;
GLint locQmatrVisu;
GLint locQmatrProj;
GLint locQTessLevelInner;
GLint locQTessLevelOuter;
GLint locQdeformer;
GLuint progL;
GLint locLVertex;
GLint locLColor;
GLint locLmatrModel;
GLint locLmatrVisu;
GLint locLmatrProj;
GLint locLTessLevelInner;
GLint locLTessLevelOuter;

GLuint vao[3];   // trois VAO, un pour chaque figure tracée
GLuint vbo[7];   // les VBO de cet exemple

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
