#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "Etat.h"
#include "Pipeline.h"
#include "Camera.h"

void calculerPhysique( )
{
    if ( Etat::enmouvement )
    {
        Etat::rtri += 0.2 * 10;
        Etat::rquad -= 0.15 * 10;
    }
}

void chargerNuanceurs()
{
    {
        // créer le programme
        progT = glCreateProgram();

        // attacher le nuanceur de sommets
        const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceursPrimT.glsl" ) };
        if ( chainesSommets[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progT, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesSommets[1];
        }
        // attacher le nuanceur de controle de la tessellation
        const GLchar *chainesTessctrl[2] = { "#version 410\n#define NUANCEUR_TESSCTRL\n", ProgNuanceur::lireNuanceur( "nuanceursPrimT.glsl" ) };
        if ( chainesTessctrl[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_CONTROL_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTessctrl, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progT, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTessctrl[1];
        }
        // attacher le nuanceur d'évaluation de la tessellation
        const GLchar *chainesTesseval[2] = { "#version 410\n#define NUANCEUR_TESSEVAL\n", ProgNuanceur::lireNuanceur( "nuanceursPrimT.glsl" ) };
        if ( chainesTesseval[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_EVALUATION_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTesseval, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progT, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTesseval[1];
        }
        // attacher le nuanceur de géometrie
        const GLchar *chainesGeometrie[2] = { "#version 410\n#define NUANCEUR_GEOMETRIE\n", ProgNuanceur::lireNuanceur( "nuanceursPrimT.glsl" ) };
        if ( chainesGeometrie[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
            glShaderSource( nuanceurObj, 2, chainesGeometrie, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progT, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesGeometrie[1];
        }
        // attacher le nuanceur de fragments
        const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceursPrimT.glsl" ) };
        if ( chainesFragments[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
            glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progT, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesFragments[1];
        }
        // faire l'édition des liens du programme
        glLinkProgram( progT );
        ProgNuanceur::afficherLogLink( progT );

        // demander la "Location" des variables
        if ( ( locTVertex = glGetAttribLocation( progT, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
        if ( ( locTColor = glGetAttribLocation( progT, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( locTmatrModel = glGetUniformLocation( progT, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
        if ( ( locTmatrVisu = glGetUniformLocation( progT, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
        if ( ( locTmatrProj = glGetUniformLocation( progT, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
        if ( ( locTTessLevelInner = glGetUniformLocation( progT, "TessLevelInner" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelInner" << std::endl;
        if ( ( locTTessLevelOuter = glGetUniformLocation( progT, "TessLevelOuter" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelOuter" << std::endl;
        if ( ( locTdeformer = glGetUniformLocation( progT, "deformer" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de deformer" << std::endl;
    }

    {
        // créer le programme
        progQ = glCreateProgram();

        // attacher le nuanceur de sommets
        const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceursPrimQ.glsl" ) };
        if ( chainesSommets[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progQ, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesSommets[1];
        }
        // attacher le nuanceur de controle de la tessellation
        const GLchar *chainesTessctrl[2] = { "#version 410\n#define NUANCEUR_TESSCTRL\n", ProgNuanceur::lireNuanceur( "nuanceursPrimQ.glsl" ) };
        if ( chainesTessctrl[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_CONTROL_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTessctrl, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progQ, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTessctrl[1];
        }
        // attacher le nuanceur d'évaluation de la tessellation
        const GLchar *chainesTesseval[2] = { "#version 410\n#define NUANCEUR_TESSEVAL\n", ProgNuanceur::lireNuanceur( "nuanceursPrimQ.glsl" ) };
        if ( chainesTesseval[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_EVALUATION_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTesseval, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progQ, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTesseval[1];
        }
        // attacher le nuanceur de géometrie
        const GLchar *chainesGeometrie[2] = { "#version 410\n#define NUANCEUR_GEOMETRIE\n", ProgNuanceur::lireNuanceur( "nuanceursPrimQ.glsl" ) };
        if ( chainesGeometrie[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
            glShaderSource( nuanceurObj, 2, chainesGeometrie, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progQ, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesGeometrie[1];
        }
        // attacher le nuanceur de fragments
        const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceursPrimQ.glsl" ) };
        if ( chainesFragments[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
            glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progQ, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesFragments[1];
        }
        // faire l'édition des liens du programme
        glLinkProgram( progQ );
        ProgNuanceur::afficherLogLink( progQ );

        // demander la "Location" des variables
        if ( ( locQVertex = glGetAttribLocation( progQ, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
        if ( ( locQColor = glGetAttribLocation( progQ, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( locQmatrModel = glGetUniformLocation( progQ, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
        if ( ( locQmatrVisu = glGetUniformLocation( progQ, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
        if ( ( locQmatrProj = glGetUniformLocation( progQ, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
        if ( ( locQTessLevelInner = glGetUniformLocation( progQ, "TessLevelInner" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelInner" << std::endl;
        if ( ( locQTessLevelOuter = glGetUniformLocation( progQ, "TessLevelOuter" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelOuter" << std::endl;
        if ( ( locQdeformer = glGetUniformLocation( progQ, "deformer" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de deformer" << std::endl;
    }

    {
        // créer le programme
        progL = glCreateProgram();

        // attacher le nuanceur de sommets
        const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceursPrimL.glsl" ) };
        if ( chainesSommets[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progL, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesSommets[1];
        }
        // attacher le nuanceur de controle de la tessellation
        const GLchar *chainesTessctrl[2] = { "#version 410\n#define NUANCEUR_TESSCTRL\n", ProgNuanceur::lireNuanceur( "nuanceursPrimL.glsl" ) };
        if ( chainesTessctrl[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_CONTROL_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTessctrl, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progL, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTessctrl[1];
        }
        // attacher le nuanceur d'évaluation de la tessellation
        const GLchar *chainesTesseval[2] = { "#version 410\n#define NUANCEUR_TESSEVAL\n", ProgNuanceur::lireNuanceur( "nuanceursPrimL.glsl" ) };
        if ( chainesTesseval[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_TESS_EVALUATION_SHADER );
            glShaderSource( nuanceurObj, 2, chainesTesseval, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progL, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesTesseval[1];
        }
        // attacher le nuanceur de géometrie
        const GLchar *chainesGeometrie[2] = { "#version 410\n#define NUANCEUR_GEOMETRIE\n", ProgNuanceur::lireNuanceur( "nuanceursPrimL.glsl" ) };
        if ( chainesGeometrie[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
            glShaderSource( nuanceurObj, 2, chainesGeometrie, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progL, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesGeometrie[1];
        }
        // attacher le nuanceur de fragments
        const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceursPrimL.glsl" ) };
        if ( chainesFragments[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
            glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progL, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesFragments[1];
        }
        // faire l'édition des liens du programme
        glLinkProgram( progL );
        ProgNuanceur::afficherLogLink( progL );

        // demander la "Location" des variables
        if ( ( locLVertex = glGetAttribLocation( progL, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
        if ( ( locLColor = glGetAttribLocation( progL, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( locLmatrModel = glGetUniformLocation( progL, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
        if ( ( locLmatrVisu = glGetUniformLocation( progL, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
        if ( ( locLmatrProj = glGetUniformLocation( progL, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
        if ( ( locLTessLevelInner = glGetUniformLocation( progL, "TessLevelInner" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelInner" << std::endl;
        if ( ( locLTessLevelOuter = glGetUniformLocation( progL, "TessLevelOuter" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelOuter" << std::endl;
    }
}

static GLint NconnecTrian, NconnecQuadrT, NconnecQuadrQ;

void FenetreTP::initialiser()
{
    // couleur de l'arrière-plan
    glm::vec4 couleurFond( 0.1, 0.1, 0.1, 1.0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );

    glEnable( GL_DEPTH_TEST );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glEnable( GL_LINE_SMOOTH );
    glLineWidth( 2.0 );

    // charger les nuanceurs
    chargerNuanceurs();

    FenetreTP::VerifierErreurGL("debut initialiser");

    // Charger le modèle
    GLfloat cooTrian[] = { 0.0,  1.0, 0.0,    // sommet 1 triangle
                          -1.0, -1.0, 0.0,    // sommet 2 triangle
                           1.0, -1.0, 0.0 };  // sommet 3 triangle
    GLfloat cooQuadr[] = {-1.0, -1.0, 0.0,    // sommet 1 carré
                           1.0, -1.0, 0.0,    // sommet 2 carré
                           1.0,  1.0, 0.0,    // sommet 3 carré
                          -1.0,  1.0, 0.0 };  // sommet 4 carré

    GLfloat coulTrian[] = { 1.0, 0.0, 0.0,    // couleurs du triangle
                            0.0, 1.0, 0.0,
                            0.0, 0.0, 1.0 };
    const GLuint connecTrian[] = { 0, 1, 2 };
    const GLuint connecQuadrT[] = { 0, 1, 2, 2, 3, 0 };
    const GLuint connecQuadrQ[] = { 0, 1, 2, 3 };

    // allouer les objets OpenGL
    glGenVertexArrays( 3, vao );
    glGenBuffers( 7, vbo );

    // initialiser le premier VAO: un triangle
    glBindVertexArray( vao[0] );
    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(cooTrian), cooTrian, GL_STATIC_DRAW );
    glVertexAttribPointer( locTVertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locTVertex);
    // charger le VBO pour les couleurs
    glBindBuffer( GL_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(coulTrian), coulTrian, GL_STATIC_DRAW );
    glVertexAttribPointer( locTColor, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locTColor);
    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[2] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connecTrian), connecTrian, GL_STATIC_DRAW ); // sizeof(connecTrian) = 3 * sizeof(uint) = 12 octets
    NconnecTrian = sizeof(connecTrian)/sizeof(GLuint); // == 3
    glBindVertexArray(0);

    // initialiser le second VAO: un quad séparé en deux triangles
    glBindVertexArray( vao[1] );
    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[3] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(cooQuadr), cooQuadr, GL_STATIC_DRAW );
    glVertexAttribPointer( locTVertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locTVertex);
    // pas de tableaux de couleurs ici (on donnera une seule couleur plus tard)
    //glDisableVertexAttribArray(locTColor); // pas nécessaire: c'est désactivé par défaut
    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[4] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connecQuadrT), connecQuadrT, GL_STATIC_DRAW );
    NconnecQuadrT = sizeof(connecQuadrT)/sizeof(GLuint); // == 6
    glBindVertexArray(0);

    // initialiser le troisième VAO: un quad
    glBindVertexArray( vao[2] );
    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[5] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(cooQuadr), cooQuadr, GL_STATIC_DRAW );
    glVertexAttribPointer( locQVertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locQVertex);
    // pas de tableaux de couleurs ici (on donnera une seule couleur plus tard)
    //glDisableVertexAttribArray(locQColor); // pas nécessaire: c'est désactivé par défaut
    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[6] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connecQuadrQ), connecQuadrQ, GL_STATIC_DRAW );
    NconnecQuadrQ = sizeof(connecQuadrQ)/sizeof(GLuint); // == 4
    glBindVertexArray(0);

    // Défaire tous les liens
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteVertexArrays( 3, vao );
    glDeleteBuffers( 7, vbo );
}

void afficherModele()
{
    // informer OpenGL du niveau de tessellation
    GLfloat TessLevelOuterTab[] = { Etat::TessLevelOuter, Etat::TessLevelOuter, Etat::TessLevelOuter, Etat::TessLevelOuter };
    GLfloat TessLevelInnerTab[] = { Etat::TessLevelInner, Etat::TessLevelInner, Etat::TessLevelInner, Etat::TessLevelInner };
    glPatchParameterfv( GL_PATCH_DEFAULT_OUTER_LEVEL, TessLevelOuterTab );
    glPatchParameterfv( GL_PATCH_DEFAULT_INNER_LEVEL, TessLevelInnerTab );

    // afin de mieux voir à l'écran, changer la largeur de ligne
    //{ int l = Etat::TessLevelInner+Etat::TessLevelOuter; glLineWidth( l < 3 ? 5. : l < 11 ? 4. : l < 17 ? 3. : l < 25 ? 2. : 1. ); }

    // 1) afficher le triangle avec son programme qui gère les triangles
    glUseProgram( progT );
    glUniform1f( locTTessLevelInner, Etat::TessLevelInner );
    glUniform1f( locTTessLevelOuter, Etat::TessLevelOuter );
    glUniform1i( locTdeformer, (int) Etat::deformer );
    glPatchParameteri( GL_PATCH_VERTICES, 3 );

    // afficher le modèle
    matrModel.LoadIdentity();
    matrModel.Translate( -1.5*2.1, 0.0, 0.0 );
    matrModel.PushMatrix();{
        matrModel.Rotate( GLfloat(Etat::rtri), 0.0, 1.0, 0.0 );
        glUniformMatrix4fv( locTmatrModel, 1, GL_FALSE, matrModel );
        glBindVertexArray( vao[0] );         // sélectionner le premier VAO
        glDrawElements( GL_PATCHES, NconnecTrian, GL_UNSIGNED_INT, 0 );
        // aurait pu être fait aussi avec glDrawArrays pour un résultat identique
        //glDrawArrays( GL_PATCHES, 0, 3 );
        glBindVertexArray( 0 );              // désélectionner le VAO
    }matrModel.PopMatrix();

    // 2) afficher le quad séparé en deux triangles

    // mettre une valeur fixe pour la couleur du rectangle
    glVertexAttrib3f( locTColor, 0.5, 0.5, 1.0 ); // semblable au glColor() de OpenGL 2.x

    matrModel.Translate( 2.1, 0.0, 0.0 );
    matrModel.PushMatrix();{
        matrModel.Rotate( GLfloat(Etat::rquad), 1.0, 0.0, 0.0 );
        glUniformMatrix4fv( locTmatrModel, 1, GL_FALSE, matrModel );
        glBindVertexArray( vao[1] );         // sélectionner le second VAO
        glDrawElements( GL_PATCHES, NconnecQuadrT, GL_UNSIGNED_INT, 0 );
        glBindVertexArray( 0 );              // désélectionner le VAO
    }matrModel.PopMatrix();

    // 3) afficher le quad avec son programme qui gère les quads
    glUseProgram( progQ );
    glUniform1f( locQTessLevelInner, Etat::TessLevelInner );
    glUniform1f( locQTessLevelOuter, Etat::TessLevelOuter );
    glUniform1i( locQdeformer, (int) Etat::deformer );
    glPatchParameteri( GL_PATCH_VERTICES, 4 );

    // mettre une valeur fixe pour la couleur du rectangle
    glVertexAttrib3f( locQColor, 0.5, 1.0, 0.5 ); // semblable au glColor() de OpenGL 2.x

    matrModel.Translate( 2.1, 0.0, 0.0 );
    matrModel.PushMatrix();{
        matrModel.Rotate( GLfloat(Etat::rquad), 1.0, 0.0, 0.0 );
        glUniformMatrix4fv( locQmatrModel, 1, GL_FALSE, matrModel );
        glBindVertexArray( vao[2] );         // sélectionner le troisième VAO
        glDrawElements( GL_PATCHES, NconnecQuadrQ, GL_UNSIGNED_INT, 0 );
        // aurait pu être fait aussi avec glDrawArrays pour un résultat identique
        //glDrawArrays( GL_PATCHES, 0, 4 );
        glBindVertexArray( 0 );              // désélectionner le VAO
    }matrModel.PopMatrix();

    // 4) afficher le quad avec son programme qui gère les lignes
    glUseProgram( progL );
    glUniform1f( locLTessLevelInner, Etat::TessLevelInner );
    glUniform1f( locLTessLevelOuter, Etat::TessLevelOuter );
    glPatchParameteri( GL_PATCH_VERTICES, 4 );

    // mettre une valeur fixe pour la couleur du rectangle
    glVertexAttrib3f( locLColor, 0.5, 1.0, 0.5 ); // semblable au glColor() de OpenGL 2.x

    matrModel.Translate( 2.1, 0.0, 0.0 );
    matrModel.PushMatrix();{
        matrModel.Rotate( GLfloat(Etat::rquad), 1.0, 0.0, 0.0 );
        glUniformMatrix4fv( locLmatrModel, 1, GL_FALSE, matrModel );
        glBindVertexArray( vao[2] );         // sélectionner le troisième VAO
        glDrawElements( GL_PATCHES, NconnecQuadrQ, GL_UNSIGNED_INT, 0 );
        // aurait pu être fait aussi avec glDrawArrays pour un résultat identique
        //glDrawArrays( GL_PATCHES, 0, 4 );
        glBindVertexArray( 0 );              // désélectionner le VAO
    }matrModel.PopMatrix();
}

void FenetreTP::afficherScene()
{
    // effacer l'ecran et le tampon de profondeur
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // mettre à jour la projection
    GLdouble aspect = (GLdouble) largeur_ / (GLdouble) hauteur_;
#if 0
    matrProj.Perspective( 25.0, aspect, 1.0, 30.0 );
#else
    matrProj.Ortho( -2.*aspect, 2.*aspect, -2., 2., 1.0, 30.0 );
#endif
    glUseProgram( progT );
    glUniformMatrix4fv( locTmatrProj, 1, GL_FALSE, matrProj );
    glUseProgram( progQ );
    glUniformMatrix4fv( locQmatrProj, 1, GL_FALSE, matrProj );
    glUseProgram( progL );
    glUniformMatrix4fv( locLmatrProj, 1, GL_FALSE, matrProj );

    // mettre à jour la caméra
    matrVisu.LookAt( camera.dist*cos(glm::radians(camera.phi))*sin(glm::radians(camera.theta)),
                     camera.dist*sin(glm::radians(camera.phi))*sin(glm::radians(camera.theta)),
                     camera.dist*cos(glm::radians(camera.theta)),
                     0.0, 0.0, 0.0,
                     0.0, 1.0, 0.0 );
    glUseProgram( progT );
    glUniformMatrix4fv( locTmatrVisu, 1, GL_FALSE, matrVisu );
    glUseProgram( progQ );
    glUniformMatrix4fv( locQmatrVisu, 1, GL_FALSE, matrVisu );
    glUseProgram( progL );
    glUniformMatrix4fv( locLmatrVisu, 1, GL_FALSE, matrVisu );

    // dessiner la scène
    afficherModele();

    FenetreTP::VerifierErreurGL("fin afficherScene");

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_ESPACE: // Mettre en pause ou reprendre l'animation
        Etat::enmouvement = !Etat::enmouvement;
        break;

    case TP_SOULIGNE:
    case TP_MOINS: // Incrémenter la distance de la caméra
        camera.dist += 0.5;
        break;

    case TP_PLUS: // Décrémenter la distance de la caméra
    case TP_EGAL:
        camera.dist -= 0.5;
        break;

    case TP_0: // Remettre les angles de la caméra à 0
        camera.theta = camera.phi = 0;
        break;

    case TP_GAUCHE: // Augmenter theta
        camera.theta += 0.5;
        break;

    case TP_DROITE: // Décrémenter theta
        camera.theta -= 0.5;
        break;

    case TP_HAUT: // Augmenter phi
        camera.phi += 0.5;
        break;

    case TP_BAS: // Décrémenter phi
        camera.phi -= 0.5;
        break;

    case TP_d: // Déformer triangle et quad
        Etat::deformer = !Etat::deformer;
        std::cout << " Etat::deformer=" << Etat::deformer << std::endl;
        break;

    case TP_r: // réinitiliaser la caméra
        Etat::rtri = Etat::rquad = 0.0;
        camera.theta = camera.phi = 0.0;
        camera.dist = 10;
        break;

    case TP_i: // Augmenter le niveau de tessellation interne
        ++Etat::TessLevelInner;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_k: // Diminuer le niveau de tessellation interne
        if ( --Etat::TessLevelInner < 1 ) Etat::TessLevelInner = 1;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_o: // Augmenter le niveau de tessellation externe
        ++Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_l: // Diminuer le niveau de tessellation externe
        if ( --Etat::TessLevelOuter < 1 ) Etat::TessLevelOuter = 1;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_u: // Augmenter les deux niveaux de tessellation
        ++Etat::TessLevelOuter;
        Etat::TessLevelInner = Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_j: // Diminuer les deux niveaux de tessellation
        if ( --Etat::TessLevelOuter < 1 ) Etat::TessLevelOuter = 1;
        Etat::TessLevelInner = Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "tess", 900, 400 );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // mettre à jour la physique
        calculerPhysique( );

        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
