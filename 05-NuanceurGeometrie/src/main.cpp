#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "Etat.h"
#include "Pipeline.h"
#include "Camera.h"

void calculerPhysique( )
{
    if ( Etat::enmouvement )
    {
        Etat::rtri += 0.2 * 10;
        Etat::rquad -= 0.15 * 10;
    }
}

void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();

    // attacher le nuanceur de sommets
    const GLchar *chainesSommets = ProgNuanceur::lireNuanceur( "nuanceurSommets.glsl" );
    if ( chainesSommets != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 1, &chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets;
    }
    // attacher le nuanceur de geometrie
    const GLchar *chainesGeometrie;
    switch ( Etat::nvues )
    {
    case 4:
    case 3: chainesGeometrie = ProgNuanceur::lireNuanceur( "nuanceurGeometrie4.glsl" ); break;
    case 2: chainesGeometrie = ProgNuanceur::lireNuanceur( "nuanceurGeometrie2.glsl" ); break;
    default: chainesGeometrie = ProgNuanceur::lireNuanceur( "nuanceurGeometrie.glsl" ); break;
    }
    if ( chainesGeometrie != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
        glShaderSource( nuanceurObj, 1, &chainesGeometrie, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesGeometrie;
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments = ProgNuanceur::lireNuanceur( "nuanceurFragments.glsl" );
    if ( chainesFragments != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 1, &chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments;
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locColor = glGetAttribLocation( prog, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
}

static GLint NconnecTrian, NconnecQuadr;

void FenetreTP::initialiser()
{
    // couleur de l'arrière-plan
    glm::vec4 couleurFond( 0.2, 0.2, 0.2, 1.0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );

    glEnable( GL_DEPTH_TEST );
    glEnable( GL_SCISSOR_TEST );
    //glEnable( GL_PROGRAM_POINT_SIZE ); // si on souhaite modifier la taille des points en cpp
    glPointSize( 10.0 );

    // charger les nuanceurs
    chargerNuanceurs();

    FenetreTP::VerifierErreurGL("debut initialiser");

    // Charger le modèle
    GLfloat cooTrian[] = { 0.0,  1.0, 0.0,    // sommet 1 triangle
                          -1.0, -1.0, 0.0,    // sommet 2 triangle
                           1.0, -1.0, 0.0 };  // sommet 3 triangle
    GLfloat cooQuadr[] = {-1.0, -1.0, 0.0,    // sommet 1 carré
                           1.0, -1.0, 0.0,    // sommet 2 carré
                           1.0,  1.0, 0.0,    // sommet 3 carré
                          -1.0,  1.0, 0.0 };  // sommet 4 carré

    GLfloat coulTrian[] = { 1.0, 0.0, 0.0,    // couleurs du triangle
                            0.0, 1.0, 0.0,
                            0.0, 0.0, 1.0 };
    const GLuint connecTrian[] = { 0, 1, 2 };
    const GLuint connecQuadr[] = { 0, 1, 2, 2, 3, 0 };

    // allouer les objets OpenGL
    glGenVertexArrays( 2, vao );
    glGenBuffers( 5, vbo );

    // initialiser le premier VAO
    glBindVertexArray( vao[0] );
    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(cooTrian), cooTrian, GL_STATIC_DRAW ); // sizeof(cooTrian) = 9 * sizeof(float) = 36 octets
    glVertexAttribPointer( locVertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locVertex);
    // charger le VBO pour les couleurs
    glBindBuffer( GL_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(coulTrian), coulTrian, GL_STATIC_DRAW );
    glVertexAttribPointer( locColor, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locColor);
    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[2] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connecTrian), connecTrian, GL_STATIC_DRAW ); // sizeof(connecTrian) = 3 * sizeof(uint) = 12 octets
    NconnecTrian = sizeof(connecTrian)/sizeof(GLuint); // == 3
    glBindVertexArray(0);

    // initialiser le second VAO
    glBindVertexArray( vao[1] );
    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[3] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(cooQuadr), cooQuadr, GL_STATIC_DRAW );
    glVertexAttribPointer( locVertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locVertex);
    // pas de tableaux de couleurs ici (on donnera une seule couleur plus tard)
    //glDisableVertexAttribArray(locColor); // pas nécessaire: c'est désactivé par défaut
    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[4] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connecQuadr), connecQuadr, GL_STATIC_DRAW );
    NconnecQuadr = sizeof(connecQuadr)/sizeof(GLuint); // == 6
    glBindVertexArray(0);

    // Défaire tous les liens
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteVertexArrays( 2, vao );
    glDeleteBuffers( 5, vbo );
}

void afficherModele()
{
    // afficher le modèle
    matrModel.LoadIdentity();

    // afficher le triangle
    matrModel.PushMatrix();{
        matrModel.Translate( -1.5, 0.0, 0.0 );
        matrModel.Rotate( GLfloat(Etat::rtri), 0.0, 1.0, 0.0 );
        matrModel.Scale( 1.5, 1.5, 1.5 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        glBindVertexArray( vao[0] );         // sélectionner le premier VAO
        glDrawElements( GL_TRIANGLES, NconnecTrian, GL_UNSIGNED_INT, 0 );
        // aurait pu être fait aussi avec glDrawArrays pour un résultat identique
        // glDrawArrays( GL_TRIANGLES, 0, 3 );
        glBindVertexArray( 0 );              // désélectionner le VAO
    }matrModel.PopMatrix();

    // mettre une valeur fixe pour la couleur du rectangle
    // GLfloat coulRectangle[] = { 0.5, 0.5, 1.0 };
    // glVertexAttrib3fv( locColor, coulRectangle );
    glVertexAttrib3f( locColor, 0.5, 0.5, 1.0 ); // semblable au glColor() de OpenGL 2.x

    // afficher le quad
    matrModel.PushMatrix();{
        matrModel.Translate( 1.5, 0.0, 0.0 );
        matrModel.Rotate( GLfloat(Etat::rquad), 1.0, 0.0, 0.0 );
        matrModel.Scale( 1.5, 1.5, 1.5 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        glBindVertexArray( vao[1] );         // sélectionner le second VAO
        glDrawElements( GL_TRIANGLES, NconnecQuadr, GL_UNSIGNED_INT, 0 );
        glBindVertexArray( 0 );              // désélectionner le VAO
    }matrModel.PopMatrix();
}

void FenetreTP::afficherScene()
{
    // effacer l'ecran et le tampon de profondeur
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glUseProgram( prog );

    // mettre à jour la projection
    GLdouble aspect = (GLdouble) largeur_ / (GLdouble) hauteur_;
    switch ( Etat::nvues )
    {
    case 4:
    case 3: matrProj.Perspective( 25.0, aspect, 1.0, 30.0 ); break;
    case 2: matrProj.Perspective( 25.0, 2.0*aspect, 1.0, 30.0 ); break;
    default: matrProj.Perspective( 25.0, aspect, 1.0, 30.0 ); break;
    }
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    // mettre à jour la caméra
    matrVisu.LookAt( camera.dist*cos(glm::radians(camera.phi))*sin(glm::radians(camera.theta)),
                     camera.dist*sin(glm::radians(camera.phi))*sin(glm::radians(camera.theta)),
                     camera.dist*cos(glm::radians(camera.theta)),
                     0.0, 0.0, 0.0,
                     0.0, 1.0, 0.0 );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    // dessiner la scène
    afficherModele();

    FenetreTP::VerifierErreurGL("fin afficherScene");

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glScissor( 0, 0, w, h );
    switch ( Etat::nvues )
    {
    default:
    case 1:
        {
            glViewport( 0, 0, w, h );
        }
        break;
    case 2:
        {
            GLsizei h2 = h/2;
            // GLfloat v[]  = {
            //    0, 0,  w, h2,
            //    0, h2, w, h2,
            // };
            // glViewportArrayv( 0, 2, v );
            // glViewportIndexedf( 0,  1,  1,  w, h2 );
            // glScissorIndexed( 0,  1,  1,  w, h2 );
            glViewport( 0, 0, w, h2 ); // pour le viewport 0
            glViewportIndexedf( 1,  0, GLfloat(h2), GLfloat(w), GLfloat(h2) ); // pour le viewport 1
            glScissorIndexed(   1,  0, h2, w, h2 );
        }
        break;
    case 3:
    case 4:
        {
            GLsizei w2 = w/2, h2 = h/2;
            // GLfloat v[]  = {
            //    0,  0,  w2, h2,
            //    w2, 0,  w2, h2,
            //    0,  h2, w2, h2,
            //    w2, h2, w2, h2,
            // };
            // glViewportArrayv( 0, 4, v );
            // glViewportIndexedf( 0,  1,  1,  W2, h2 );
            // glScissorIndexed( 0,  1,  1,  W2, h2 );
            glViewport( 0, 0, w2, h2 ); // pour le viewport 0
            glViewportIndexedf( 1,  GLfloat(w2), 0,  GLfloat(w2), GLfloat(h2) ); // pour le viewport 1
            glScissorIndexed(   1,  w2, 0,  w2, h2 );
            glViewportIndexedf( 2,  0,  GLfloat(h2), GLfloat(w2), GLfloat(h2) ); // pour le viewport 2
            glScissorIndexed(   2,  0,  h2, w2, h2 );
            glViewportIndexedf( 3,  GLfloat(w2), GLfloat(h2), GLfloat(w2), GLfloat(h2) ); // pour le viewport 3
            glScissorIndexed(   3,  w2, h2, w2, h2 );
        }
        break;
    }
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_ESPACE: // Mettre en pause ou reprendre l'animation
        Etat::enmouvement = !Etat::enmouvement;
        break;

    case TP_SOULIGNE:
    case TP_MOINS: // Incrémenter la distance de la caméra
        camera.dist += 0.5;
        break;

    case TP_PLUS: // Décrémenter la distance de la caméra
    case TP_EGAL:
        camera.dist -= 0.5;
        break;

    case TP_0: // Remettre les angles de la caméra à 0
        camera.theta = camera.phi = 0;
        break;

    case TP_GAUCHE: // Augmenter theta
        camera.theta += 0.5;
        break;

    case TP_DROITE: // Décrémenter theta
        camera.theta -= 0.5;
        break;

    case TP_HAUT: // Augmenter phi
        camera.phi += 0.5;
        break;

    case TP_BAS:  // Décrémenter phi
        camera.phi -= 0.5;
        break;

    case TP_r: // réinitiliaser la caméra
        Etat::rtri = Etat::rquad = 0.0;
        camera.theta = camera.phi = 0.0;
        camera.dist = 10;
        break;

    case TP_1: // utilisewr 1 seule clôture
        Etat::nvues = 1;
        std::cout << " Etat::nvues=" << Etat::nvues << std::endl;
        chargerNuanceurs();
        redimensionner( largeur_, hauteur_  );
        break;

    case TP_2: // utilisewr 2 clôtures
        Etat::nvues = 2;
        std::cout << " Etat::nvues=" << Etat::nvues << std::endl;
        chargerNuanceurs();
        redimensionner( largeur_, hauteur_  );
        break;

    case TP_4: // utilisewr 4 clôtures
        Etat::nvues = 4;
        std::cout << " Etat::nvues=" << Etat::nvues << std::endl;
        chargerNuanceurs();
        redimensionner( largeur_, hauteur_  );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "geom" );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // mettre à jour la physique
        calculerPhysique( );

        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
