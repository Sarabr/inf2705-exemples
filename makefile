all : clean liens

###
### Les touches
###

# créer les fichiers touches.txt (pour l'aide)
touches :
	for t in *-*/; do ( cd $$t; ls src/*.cpp 2>/dev/null 1>&2 && ../bin/touches.sh || true ); done
	chmod 644 */src/touches.txt
# voir l'utilisation des touches dans les exemples
touchesUtil :
	cat */src/touches.txt | sort -u

###
### Les fichiers communs
###

liens :
	for f in `echo srcCommun/*.h [0-9]*/*{.h,.cpp,.glsl}`; do ln -sf ../highlight.php $$f.php; git add $$f.php; done
liensrm :
	for f in `echo srcCommun/*.h [0-9]*/*{.h,.cpp,.glsl}`; do git rm $$f.php; done

communexem :
	for f in *-*/s*/inf2705-*.h; do diff -s srcCommun/`basename $$f` $$f || cp -p -i $$f srcCommun/`basename $$f`; done
communsrc :
	for f in *-*/s*/inf2705-*.h; do diff -s $$f srcCommun/`basename $$f` || cp -p -i srcCommun/`basename $$f` $$f; done

clean : ; rm -rf *-*/s??/{build-*,Debug,Release,x64,.vs,core}
realclean : clean ; rm -rf *-*/s??/packages

exe : ; for d in *-*/src/main.cpp; do ( cd `dirname $$d` && make exe ); done
