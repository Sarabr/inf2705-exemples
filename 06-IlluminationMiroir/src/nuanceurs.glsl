// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////

// Définition des paramètres des sources de lumière
layout (std140) uniform LightSourceParameters
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position;      // dans le repère du monde
    vec3 spotDirection; // dans le repère du monde
    float spotExponent;
    float spotAngleOuverture; // ([0.0,90.0] ou 180.0)
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
} LightSource;

// Définition des paramètres des matériaux
layout (std140) uniform MaterialParameters
{
    vec4 emission;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} FrontMaterial;

// Définition des paramètres globaux du modèle de lumière
layout (std140) uniform LightModelParameters
{
    vec4 ambient;       // couleur ambiante
    bool localViewer;   // observateur local ou à l'infini?
    bool twoSide;       // éclairage sur les deux côtés ou un seul?
} LightModel;

////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform mat3 matrNormale;

uniform vec4 planCoupe; // équation du plan de coupe

layout(location=0) in vec4 Vertex;
layout(location=2) in vec3 Normal;
layout(location=3) in vec4 Color;

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

out Attribs {
    vec3 lumiDir;
    vec3 normale, obsVec;
    vec4 couleur;
} AttribsOut;

void main(void)
{
    // appliquer la transformation standard du sommet (P * V * M * sommet)
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // Initialiser la variable gl_ClipDistance[] pour que le découpage soit fait par OpenGL
    {
        vec4 pos = matrModel * Vertex;
        gl_ClipDistance[0] = dot( planCoupe, pos );
    }

    // assigner la couleur du sommet
    AttribsOut.couleur = Color;

    // calculer la normale (N) qui sera interpolée pour le nuanceur de fragments
    AttribsOut.normale = matrNormale * Normal;

    // calculer la position (P) du sommet (dans le repère de la caméra)
    vec3 pos = vec3( matrVisu * matrModel * Vertex );

    // calculer le vecteur de la direction (L) de la lumière (dans le repère de la caméra)
    AttribsOut.lumiDir = ( matrVisu * LightSource.position ).xyz - pos;

    // calculer le vecteur de la direction (O) vers l'observateur (dans le repère de la caméra)
    AttribsOut.obsVec = ( LightModel.localViewer ?
                          (-pos) :        // =(0-pos) un vecteur qui pointe vers le (0,0,0), c'est-à-dire vers la caméra
                          vec3( 0.0, 0.0, 1.0 ) ); // on considère que l'observateur (la caméra) est à l'infini dans la direction (0,0,1)

    // calculer le vecteur de la direction du spot (dans le repère de la caméra)
    //AttribsOut.spotDir = transpose(inverse(mat3(matrVisu))) * -LightSource.spotDirection;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

bool utiliseBlinn = false;

in Attribs {
    vec3 lumiDir;
    vec3 normale, obsVec;
    vec4 couleur;
} AttribsIn;

out vec4 FragColor;

float attenuation = 1.0;
vec4 calculerReflexion( in vec3 L, in vec3 N, in vec3 O )
{
    vec4 coul = vec4(0.0);

    // calculer la composante ambiante pour la source de lumière
    coul += FrontMaterial.ambient * LightSource.ambient;

    // calculer l'éclairage seulement si le produit scalaire est positif
    float NdotL = max( 0.0, dot( N, L ) );
    if ( NdotL > 0.0 )
    {
        // calculer la composante diffuse
        // (dans cet exemple, on utilise plutôt la couleur de l'objet au lieu de FrontMaterial.diffuse)
        //coul += attenuation * FrontMaterial.diffuse * LightSource.diffuse * NdotL;
        coul += attenuation * AttribsIn.couleur * LightSource.diffuse * NdotL;

        // calculer la composante spéculaire (Blinn ou Phong : spec = BdotN ou RdotO )
        float spec = ( utiliseBlinn ?
                       dot( normalize( L + O ), N ) : // dot( B, N )
                       dot( reflect( -L, N ), O ) ); // dot( R, O )
        if ( spec > 0 ) coul += FrontMaterial.specular * LightSource.specular * pow( spec, FrontMaterial.shininess );
    }

    return( coul );
}

void main(void)
{
    vec3 L = normalize( AttribsIn.lumiDir ); // vecteur vers la source lumineuse
    //vec3 N = normalize( AttribsIn.normale ); // vecteur normal
    vec3 N = normalize( gl_FrontFacing ? AttribsIn.normale : -AttribsIn.normale );
    vec3 O = normalize( AttribsIn.obsVec );  // position de l'observateur

    vec4 coul = calculerReflexion( L, N, O );

    // utiliser le .alpha de la couleur courante (essentiellement pour le miroir transparent)
    coul.a = AttribsIn.couleur.a;

    // seuiller chaque composante entre 0 et 1 et assigner la couleur finale du fragment
    FragColor = clamp( coul, 0.0, 1.0 );

    // Pour « voir » les normales, on peut remplacer la couleur du fragment par la normale.
    // (Les composantes de la normale variant entre -1 et +1, il faut
    // toutefois les convertir en une couleur entre 0 et +1 en faisant (N+1)/2.)
    //FragColor = clamp( vec4( (N+1)/2, AttribsIn.couleur.a ), 0.0, 1.0 );
}

#endif
