/////////////////////////
// Nuanceur de sommets //
/////////////////////////

//#version 130
//out vec3 normal, lumidDir, obsVec;
varying vec3 normal, lumidDir, obsVec;

void main()
{
    // appliquer la transformation standard du sommet (ModelView et Projection)
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

    // calculer la normale qui sera interpolée pour le nuanceur de fragments
    normal = gl_NormalMatrix * gl_Normal;
    //normal = normalize( gl_NormalMatrix * gl_Normal );

    // calculer la position du sommet dans le repère de la caméra ("repère de l'observateur")
    vec3 pos = vec3( gl_ModelViewMatrix * gl_Vertex );

    // calculer le vecteur de la direction de la lumière (En OpenGl 2.x, gl_LightSource[0].position est déjà dans le repère de la caméra)
    lumidDir = vec3( gl_LightSource[0].position.xyz - pos );
    obsVec = (-pos); // vecteur qui pointe vers le (0,0,0), c'est-à-dire vers la caméra
}

///////////////////////////
// Nuanceur de fragments //
///////////////////////////

//#version 130
//in vec3 normal, lumidDir, obsVec;
varying vec3 normal, lumidDir, obsVec;

void main (void)
{
    // calculer la composante ambiante
    vec4 couleur = (FrontMaterial.emission + FrontMaterial.ambient * LightModel.ambient) +
    gl_LightSource[0].ambient * gl_FrontMaterial.ambient;

    // calculer le vecteur normal
    vec3 N = normalize( normal );

    // direction de la lumière
    // calculer le vecteur de la surface vers la source lumineuse
    // normaliser le vecteur de la surface vers la source lumineuse
    vec3 L = normalize( lumidDir );

    // calculer le produit scalaire pour la réflexion diffuse
    // normale . direction de la lumière
    float NdotL = dot( N, L );

    // calcul de l'éclairage seulement si le produit scalaire est positif
    if ( NdotL > 0.0 )
    {
        // calculer la composante diffuse
        couleur += gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * NdotL;

        // calculer la composante spéculaire
        vec3 O = normalize( obsVec );
        vec3 R = reflect( -L, N ); // réflexion de L par rapport à N
        // calculer le produit scalaire pour la réflexion spéculaire
        float NdotHV = max( dot( R, O ), 0.0 );
        couleur += gl_FrontMaterial.specular * gl_LightSource[0].specular * pow( NdotHV, gl_FrontMaterial.shininess );
    }

    gl_FragColor = clamp( couleur, 0.0, 1.0 );
}
