#include <stdlib.h>
#include <string.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-texture.h"
#include "Etat.h"
#include "Pipeline.h"

void chargerTextures()
{
    unsigned char *pixels;
    GLsizei largeur, hauteur;
    if ( ( pixels = ChargerImage( "textures/lutin.bmp", largeur, hauteur ) ) != NULL )
    {
        glGenTextures( 1, &Etat::textureLUTIN );
        glBindTexture( GL_TEXTURE_2D, Etat::textureLUTIN );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, largeur, hauteur, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glBindTexture( GL_TEXTURE_2D, 0 );
        delete[] pixels;
    }
    glBindTexture( GL_TEXTURE_2D, Etat::textureECHIQUIER );
    if ( ( pixels = ChargerImage( "textures/echiquier.bmp", largeur, hauteur ) ) != NULL )
    {
        glGenTextures( 1, &Etat::textureECHIQUIER );
        glBindTexture( GL_TEXTURE_2D, Etat::textureECHIQUIER );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, largeur, hauteur, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glBindTexture( GL_TEXTURE_2D, 0 );
        delete[] pixels;
    }
}

void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();

    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }
    // attacher le nuanceur de géometrie
    const char *preambule = ( Etat::affichePoints ?
                              "#version 410\n#define NUANCEUR_GEOMETRIE_POINTS\n" :
                              "#version 410\n#define NUANCEUR_GEOMETRIE_LINES\n" );
    const GLchar *chainesGeometrie[2] = { preambule, ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesGeometrie[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
        glShaderSource( nuanceurObj, 2, chainesGeometrie, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesGeometrie[1];
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesFragments[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments[1];
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    glUseProgram( prog );
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locColor = glGetAttribLocation( prog, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    if ( ( loclaTexture = glGetUniformLocation( prog, "laTexture" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTexture" << std::endl;
    if ( ( loctexnumero = glGetUniformLocation( prog, "texnumero" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de texnumero" << std::endl;
    if ( ( locpointsize = glGetUniformLocation( prog, "pointsize" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de pointsize" << std::endl;

    FenetreTP::VerifierErreurGL("fin chargerNuanceurs");
}

static const GLint Nsommets = 6;

void FenetreTP::initialiser()
{
    glm::vec4 couleurFond( 0.1, 0.1, 0.1, 0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_PROGRAM_POINT_SIZE );

    chargerTextures();
    chargerNuanceurs();
    FenetreTP::VerifierErreurGL("debut initialiser");

    GLfloat sommets1[3*Nsommets] = {-0.4, 0.4, 0.0,
                                     0.0, 0.4, 0.0,
                                     0.4, 0.4, 0.0,
                                    -0.4, 0.0, 0.0,
                                     0.0, 0.0, 0.0,
                                     0.4, 0.0, 0.0 };
    GLfloat couleur1[3*Nsommets] = { 1.0, 0.0, 0.0,
                                     0.0, 1.0, 0.0,
                                     0.0, 0.0, 1.0,
                                     0.0, 1.0, 1.0,
                                     1.0, 0.0, 1.0,
                                     1.0, 1.0, 0.0 };

    // allouer les objets OpenGL
    glGenVertexArrays( 1, vao );
    glBindVertexArray( vao[0] );
    glGenBuffers( 2, vbo );

    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(sommets1), sommets1, GL_STATIC_DRAW );
    glVertexAttribPointer( locVertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locVertex);
    // charger le VBO pour les couleurs
    glBindBuffer( GL_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(couleur1), couleur1, GL_STATIC_DRAW );
    glVertexAttribPointer( locColor, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locColor);

    // Défaire tous les liens
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteVertexArrays( 1, vao );
    glDeleteBuffers( 2, vbo );
}

void FenetreTP::afficherScene()
{
    FenetreTP::VerifierErreurGL("debut afficherScene");
    glUseProgram( prog );
    glUniform1i( loclaTexture, 0 );   // '0' => dans l'unité de texture 0
    glUniform1i( loctexnumero, Etat::texnumero );
    glUniform1f( locpointsize, Etat::pointsize );

    glActiveTexture( GL_TEXTURE0 ); // l'unité de texture 0
    switch ( Etat::texnumero )
    {
    default: glBindTexture( GL_TEXTURE_2D, 0 ); break; // pas de texture
    case 1: glBindTexture( GL_TEXTURE_2D, Etat::textureLUTIN ); break;
    case 2: glBindTexture( GL_TEXTURE_2D, Etat::textureECHIQUIER ); break;
    }

    MatricePipeline matrModel, matrVisu, matrProj;

    // définir le pipeline graphique
    matrProj.LoadIdentity();
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    matrVisu.LoadIdentity();
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    matrModel.LoadIdentity();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

    // afficher
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glDrawArrays( GL_POINTS, 0, Nsommets );

    FenetreTP::VerifierErreurGL("fin afficherScene");

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_p: // Afficher des points ou des panneaux
        Etat::affichePoints = !Etat::affichePoints;
        std::cout << " Etat::affichePoints=" << Etat::affichePoints << std::endl;
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_t: // Changer la texture utilisée: 0-aucune, 1-lutin, 2-echiquier
        if ( ++Etat::texnumero > 2 ) Etat::texnumero = 0;
        std::cout << " Etat::texnumero=" << Etat::texnumero << std::endl;
        break;

    case TP_GAUCHE: // Diminuer la taille des points
        Etat::pointsize -= 0.2;
        if ( Etat::pointsize < 1.0 ) Etat::pointsize = 1.0;
        std::cout << " Etat::pointsize=" << Etat::pointsize << std::endl;
        break;

    case TP_DROITE: // Augmenter la taille des points
        Etat::pointsize += 0.2;
        std::cout << " Etat::pointsize=" << Etat::pointsize << std::endl;
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}
void FenetreTP::sourisMolette( int x, int y )
{
}
void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "panneau", 400, 400 );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
